//
//  NSDate+Category.h
//  Number26Challenge
//
//  Created by A. J. on 30/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Category)

+ (NSDate *)subtractNumberOfDays:(int)numberOfDays;
+ (NSString *)apiFormatDate:(NSDate *)date;
+ (NSDate *)dateFromString:(NSString *)dateString;
- (NSString *)dayAsString;
- (NSString *)dateStringToShow;
+ (NSDate *)dateWithNoHours:(NSDate *)date;

@end
