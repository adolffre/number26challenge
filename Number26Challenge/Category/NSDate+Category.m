//
//  NSDate+Category.m
//  Number26Challenge
//
//  Created by A. J. on 30/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "NSDate+Category.h"

@implementation NSDate (Category)

#pragma mark - Editing Dates
+ (NSDate *)subtractNumberOfDays:(int)numberOfDays
{
    NSDate *now = [NSDate date];
    NSDate *returnDate = [now dateByAddingTimeInterval:-numberOfDays*24*60*60];
    return returnDate;
}

+ (NSDate *)dateWithNoHours:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

#pragma mark - Date from String
+ (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

#pragma mark - String from Date
+ (NSString *)apiFormatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:date] ;
    return dateString;
}

- (NSString *)dayAsString
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:self] ;
    return [dateString substringFromIndex:8];
}

-(NSString *)dateStringToShow
{
    NSString *dateString = [self dayAsString];
    if( [[NSCalendar currentCalendar] isDateInToday:self])
    {
        dateString = @"today";
    }
    return dateString;
}

@end
