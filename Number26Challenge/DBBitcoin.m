//
//  DBBitcoin.m
//  Number26Challenge
//
//  Created by A. J. on 30/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "DBBitcoin.h"
#import "Network+Bitcoin.h"
#import "NSDate+Category.h"

@implementation DBBitcoin

#pragma mark - Dictionary Parser
- (void)startBitcoinWithDict:(NSDictionary *)dictionary
{
    NSDictionary *rateDictionary = dictionary[@"bpi"][@"EUR"];
    NSDecimalNumber *rate = [NSDecimalNumber decimalNumberWithDecimal:[rateDictionary[@"rate_float"] decimalValue]];
    self.date = [NSDate dateWithNoHours:[NSDate date]];
    self.rate = rate;
    self.currency = @"EUR";

}

+ (NSDictionary *)parseJsonToBitcoins:(NSDictionary *)dictionary
{
    NSMutableDictionary *allRates = [NSMutableDictionary new];
    
    NSArray *keys = [dictionary[@"bpi"] allKeys];
    
    for (int i =0; i<keys.count; i++) {
        NSString *date = keys[i];
        NSDecimalNumber *rate = [NSDecimalNumber decimalNumberWithDecimal:[dictionary[@"bpi"][date] decimalValue]];
        DBBitcoin *bitcoin = [[[Network sharedManager] database] newBitcoin];
        bitcoin.date = [NSDate dateWithNoHours:[NSDate dateFromString:date]];
        bitcoin.rate = rate;
        bitcoin.currency = @"EUR";
        allRates[date] = bitcoin;
    }
    
    return [NSDictionary dictionaryWithDictionary:allRates];
}
@end
