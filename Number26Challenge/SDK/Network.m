//
//  Requester.m
//  Number26Challenge
//
//  Created by A. J. on 29/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Network.h"

@implementation Network

#pragma mark - Singleton Methods
+ (instancetype)sharedManager
{
    static Network *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.apiUrl = @"https://api.coindesk.com/v1/bpi";
        sharedMyManager.database = [DatabaseManager sharedManager];
    });
    return sharedMyManager;
}

#pragma mark - Requests
- (void)getDataUsingRequest:(NSMutableURLRequest *)request withCompletionHandler:(GetRates)completion
{
    // Execute Request
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse =(NSHTTPURLResponse *)response;
        if(error || httpResponse.statusCode !=200){
            completion(nil,error);
        }else{
            NSError *err = nil;
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingMutableContainers
                                                                             error:&err];
            completion(jsonDictionary,nil);
        }
    }];
    [task resume];
    
}

#pragma mark - error
- (NSString *)showErrorMessage:(NSError *)error
{
    return error.localizedDescription;
}
@end
