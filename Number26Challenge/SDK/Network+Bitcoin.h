//
//  Network+Bitcoin.h
//  Number26Challenge
//
//  Created by A. J. on 29/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//
#import "Network.h"
#import <Foundation/Foundation.h>

@interface Network (Bitcoin)
/**
 *  Get last rate from today
 *
 *  @param completion as above
 */
- (void)getLastUpdateWithCompletionHandler:(GetRates)completion;

/**
 *  Get x rate days
 *
 *  @param completion as above
 */
- (void)getLastAnyDays:(int)numberOfDays WithCompletionHandler:(GetRates)completion;

@end
