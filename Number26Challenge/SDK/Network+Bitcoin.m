//
//  Network+Bitcoin.m
//  Number26Challenge
//
//  Created by A. J. on 29/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Network+Bitcoin.h"
#import "DBBitcoin.h"
#import "NSDate+Category.h"

@implementation Network (Bitcoin)

#pragma mark - Requests
- (void)getLastAnyDays:(int)numberOfDays WithCompletionHandler:(GetRates)completion
{
    NSString *requestUrl = [self getApiStringLastAnyDays:numberOfDays];
    // Request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:requestUrl]];
    [[Network sharedManager] getDataUsingRequest:request withCompletionHandler:^(NSDictionary *response , NSError *error) {
        if(error)
        {
            completion(nil, error);
        }else
        {
            [[Network sharedManager] getLastUpdateWithCompletionHandler:^(NSDictionary *response1, NSError *error) {
                if(error)
                {
                  completion(nil, error);
                }
                else
                {
                    NSDictionary *rates = [self addingLastDay:response1 ToRates:[DBBitcoin parseJsonToBitcoins:response]];
                    completion(rates,nil);
                }
            }];

            
        }
        
    }];

}

- (void)getLastUpdateWithCompletionHandler:(GetRates)completion
{
    NSString *requestUrl = [self getApiStringLastUpdate];
    // Request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:requestUrl]];
    [[Network sharedManager] getDataUsingRequest:request withCompletionHandler:^(NSDictionary *response , NSError *error) {
        if(error)
        {
            completion(nil, error);
        }else
        {
            DBBitcoin *bitcoin;
            BOOL needToSave = NO;
            NSArray *rate = [[[Network sharedManager] database]getRateFromDate:nil];
            if(rate.count)
            {
                bitcoin = [rate firstObject];
                needToSave = YES;
            }
            else
            {
                bitcoin = [[[Network sharedManager] database] newBitcoin];
            }
           
            [bitcoin startBitcoinWithDict:response];
            NSString *dateString = [NSDate apiFormatDate:bitcoin.date];
            NSDictionary *dictionary = @{  dateString : bitcoin
                                        };
            if(needToSave){
                [[[Network sharedManager] database] save];
            }
            completion(dictionary,nil);
        }

    }];
    
}

#pragma mark - URL String
- (NSString *)getApiStringLastAnyDays:(int)numberOfDays
{
    NSDate *fromDate = [NSDate subtractNumberOfDays:numberOfDays];
    NSString *fromDateString = [NSDate apiFormatDate:fromDate];
    NSString *toDateString = [NSDate apiFormatDate:[NSDate date]];
    NSString *currency = [self getApiStringWithCurrency:@"EUR"];
    NSString *returnString = [NSString stringWithFormat:@"%@/historical/close.json%@&start=%@&end=%@",self.apiUrl ,currency,fromDateString, toDateString];
    return returnString;
}

- (NSString *)getApiStringLastUpdate
{
    NSString *returnString = [NSString stringWithFormat:@"%@/currentprice.json",self.apiUrl];
    return returnString;
}

- (NSString *)getApiStringWithCurrency:(NSString *)currency
{
    return [NSString stringWithFormat:@"?currency=%@",currency];
}

#pragma mark - Merge Dictionaries
- (NSDictionary *)addingLastDay:(NSDictionary *)lastDayDictionary ToRates:(NSDictionary *)dictionary
{
    
    NSString *key = [[lastDayDictionary allKeys] firstObject];
    NSMutableDictionary *returnRates = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    returnRates[key] = lastDayDictionary[key];
    
    return returnRates;
}

@end
