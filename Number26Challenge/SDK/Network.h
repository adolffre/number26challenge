//
//  Network.h
//  Number26Challenge
//
//  Created by A. J. on 29/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseManager.h"

@interface Network : NSObject
/**
 *  GetRates completion
 *
 *  @param responde          Response dictonary
 *  @param error             Error for failure
 */
typedef void (^GetRates)(NSDictionary *response, NSError *error);

#pragma mark - Properties
/**
 *  Api url
 */
@property (nonatomic) NSString *apiUrl;
/**
 *  Database
 */
@property (nonatomic) DatabaseManager *database;

#pragma mark - Singleton

+ (instancetype)sharedManager;

#pragma mark - Request
/**
 *  Request
 *
 *  @param completion as above
 */
- (void)getDataUsingRequest:(NSMutableURLRequest *)request withCompletionHandler:(GetRates)completion;
/**
 *  Return string error from error
 *
 *  @param error        Error for failure
 */
- (NSString *)showErrorMessage:(NSError *)error;
@end
