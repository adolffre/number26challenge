//
//  ViewController.m
//  Number26Challenge
//
//  Created by A. J. on 29/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "ViewController.h"
#import <AmChartsMobile/AmCharts.h>
#import "Network+Bitcoin.h"
#import "NSDate+Category.h"

@interface ViewController ()
#pragma mark - Properties
@property (nonatomic, weak) IBOutlet AmMobileChartView *chartView;
@property (nonatomic, strong) NSMutableArray *dataProvider;
@property (nonatomic, strong) NSTimer *updateTimer;
@property (nonatomic, strong) NSArray *rates;
@property (nonatomic, strong) UIAlertController *alertViewController;
@property (nonatomic) BOOL isNetwork;

@end

@implementation ViewController
#pragma mark - ViewController life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:60
                                                    target:self
                                                  selector:@selector(updateTimerFired:)
                                                  userInfo:nil
                                                   repeats:YES];
    if([self showRatesFromDB])
    {
        [self updateDataProvider:self.rates];
        self.isNetwork = NO;
    }
    [self getNewRates];
}

#pragma mark - UpdateView
- (void)createChart{
    AmSerialChart *chart = [[AmSerialChart alloc] init];
    chart.type = @"serial";
    chart.dataProvider = [self.dataProvider mutableCopy];
    chart.categoryField = @"date";
    chart.categoryAxis.gridPosition = @"start";
    chart.categoryAxis.axisColor = @"#DADADA";
    AmValueAxis *valAxis = [[AmValueAxis alloc] init];
    valAxis.axisAlpha = @(0.2);
    chart.valueAxes = [@[valAxis] mutableCopy];
    chart.balloon = nil;
    AmGraph *graph = [[AmGraph alloc] init];
    graph.type = @"column";
    graph.title = @"Rate";
    graph.valueField = @"rate";
    graph.lineAlpha = @(0);
    graph.fillColors = @"#ADD981";
    graph.fillAlphas = @(0.8);
    graph.balloonText = @"[[title]] in [[category]]:<b>€[[value]]</b>";
    chart.graphs = [@[graph] mutableCopy];

    [self saveAndClean];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.chartView setChart:chart];
        [self.chartView drawChart];
        
    });

}

- (BOOL)showRatesFromDB{
    self.rates = [[[Network sharedManager] database] allRates];
    return self.rates.count ? YES : NO;
}

#pragma mark - Custom methods
- (void)saveAndClean{
    if(self.isNetwork)
    {
        [[[Network sharedManager] database] removeAllRates];
        [[[Network sharedManager] database] save];
    }
}

- (void)createDataProviderFromDictionary:(NSDictionary *)dictionary
{
    NSArray *sortedArray = [self sortedArrayOfKeysFromDictionary: dictionary];
    NSMutableArray *dataProvider  = [NSMutableArray new];
    
    int i =0;
    for (NSString *key in sortedArray) {
        NSDecimalNumber *rate = [(DBBitcoin *)dictionary[key] rate];
        NSDate *date = [(DBBitcoin *)dictionary[key] date];
        dataProvider[i] = @{@"date" : [date dateStringToShow] , @"rate" : rate};
        i++;
    }
    self.dataProvider = dataProvider;
}

- (void)updateDataProvider:(NSArray *)rates
{
    NSMutableArray *ratesArray = [NSMutableArray new];
    int i = 0;
    for (DBBitcoin *bitcoin in rates) {
        NSDecimalNumber *rate = [bitcoin rate];
        ratesArray[i]= @{
                         @"date" : [bitcoin.date dateStringToShow],
                         @"rate" : rate
                         };
        i++;
    }
    self.dataProvider = ratesArray;
    [self createChart];
    
}

- (void)updateLastRateFromDictionary:(NSDictionary *)dictionary
{
    NSString *key = [[dictionary allKeys] firstObject];
    NSDate *date = [(DBBitcoin *)dictionary[key] date];
    NSDecimalNumber *rate = [(DBBitcoin *)dictionary[key] rate];
    self.dataProvider[self.dataProvider.count-1] = @{
                                                     @"date" : [date dateStringToShow],
                                                     @"rate" : rate
                                                     };
    
}

- (NSArray *)sortedArrayOfKeysFromDictionary:(NSDictionary *)dictionary
{
    NSArray *sortedArray = [[dictionary allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [[dictionary[obj1]date] compare:[dictionary[obj2]date]];
    }];
    return sortedArray;
}

#pragma mark - Request(s)
- (void)updateTimerFired:(NSTimer *)sender
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        [[Network sharedManager] getLastUpdateWithCompletionHandler:^(NSDictionary *response, NSError *error) {
            if(error)
            {
                [self showErrorMessage:[[Network sharedManager] showErrorMessage:error]];
            }
            else
            {
                [self updateLastRateFromDictionary:response];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.chartView.chart setDataProvider:[self.dataProvider mutableCopy]];
                    [self.chartView drawChart];
                });
            }
        }];
    });
}

- (void)getNewRates
{
    self.chartView.clearsContextBeforeDrawing = YES;
    [[Network sharedManager] getLastAnyDays:28 WithCompletionHandler:^(NSDictionary *response, NSError *error) {
        if(error)
        {
            [self showErrorMessage:[[Network sharedManager] showErrorMessage:error]];
        }
        else
        {
            self.isNetwork = YES;
            [self createDataProviderFromDictionary:response];
            [self createChart];
        }
    }];
    
}

#pragma mark - Error Message
- (void)showErrorMessage:(NSString *)errorMessage{
    
    if(self.alertViewController)
    {
        [self.alertViewController dismissViewControllerAnimated:NO completion:nil];
    }
    self.alertViewController = [UIAlertController alertControllerWithTitle:@"ooppss..."
                                                                   message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) { }];
    [self.alertViewController addAction:cancelAction];
    [self presentViewController:self.alertViewController animated:YES completion:nil];
}

#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
