//
//  DatabaseManager.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBBitcoin+CoreDataProperties.h"

@interface DatabaseManager : NSObject

#pragma mark - Singleton

+ (instancetype)sharedManager;

#pragma mark - Properties

@property (nonatomic) NSManagedObjectContext *context;

#pragma mark - Create

- (DBBitcoin *)newBitcoin;

#pragma mark - Read

- (NSArray *)allRates;
- (NSArray *)getRateFromDate:(NSDate *)date;
#pragma mark - Update

- (BOOL)save;

#pragma mark - Delete

- (BOOL)removeRecord:(NSManagedObject *)record;
- (BOOL)removeAllRates;


@end
