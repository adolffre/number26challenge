//
//  DBBitcoin.h
//  Number26Challenge
//
//  Created by A. J. on 30/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DBBitcoin : NSManagedObject

+ (NSDictionary *)parseJsonToBitcoins:(NSDictionary *)dictionary;
- (void)startBitcoinWithDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "DBBitcoin+CoreDataProperties.h"
