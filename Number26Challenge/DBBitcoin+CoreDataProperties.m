//
//  DBBitcoin+CoreDataProperties.m
//  Number26Challenge
//
//  Created by A. J. on 30/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DBBitcoin+CoreDataProperties.h"

@implementation DBBitcoin (CoreDataProperties)

@dynamic rate;
@dynamic currency;
@dynamic date;

@end
