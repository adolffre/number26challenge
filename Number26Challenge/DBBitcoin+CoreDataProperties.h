//
//  DBBitcoin+CoreDataProperties.h
//  Number26Challenge
//
//  Created by A. J. on 30/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DBBitcoin.h"

NS_ASSUME_NONNULL_BEGIN

@interface DBBitcoin (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDecimalNumber *rate;
@property (nullable, nonatomic, retain) NSString *currency;
@property (nullable, nonatomic, retain) NSDate *date;

@end

NS_ASSUME_NONNULL_END
