//
//  main.m
//  Number26Challenge
//
//  Created by A. J. on 29/06/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
